# stackup-day5-secondary
StackUp Day5 Node.js practice.
This is a practice app during the NUS Full Stack Foundation course.

## How to get it up and running
* `npm install`
* Start server with `node server/app.js` or `nodemon`
* Visit the localhost port indicated on your server.
