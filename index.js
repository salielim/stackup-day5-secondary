const express = require("express");
const app = express();

port = 3000;
if (process.argv[2]) {
    port = +process.argv[2];
}

app.use(
    express.static(__dirname + '/public')
);

// Display random image on img

// Get list of filenames in img folder
const imgFolder = "./public/img";
const fs = require("fs"); // lib to get filenames in folder
const img = fs.readdirSync(imgFolder); // array of img filenames
console.log(img);

app.get("/randomimg", function(req, res) {
    const randImg = img[Math.floor(Math.random()*img.length)];
    console.log(randImg);

    res.status(200);
    res.type("image/jpg");
    res.sendFile(randImg, { root: __dirname + '/public/img' });
    // OR res.send("<img src=...>")
});

app.listen(
    port,
    function() {
    console.log("application started on port %d", port);
    }
);